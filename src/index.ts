/** Imports **/
import 'reflect-metadata';
import {createConnection, getConnectionOptions} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as helmet from "helmet";
import * as cors from "cors";
import routes from "./routes";
import {NamingStrategyCRM} from "./config/namingStrategyCRM";
import * as http from 'http';
/** Config **/
// Create a new express application instance
const app = express();
// Se necessita usar el http para socket io
const httpServer = http.createServer(app);

getConnectionOptions().then(connectionOptions => {
    return createConnection(Object.assign(connectionOptions, {
        namingStrategy: new NamingStrategyCRM()
    }))
});

// /** Connects to the Database -> then starts the express **/
createConnection()
    .then(async connection => {
        /** Settings **/
        // Para que el puerto corra en el 8080 o en el que el servidor le de por default
        const puerto = process.env.PORT || 8080;
        app.set('port', puerto);
        // Call midlewares
        app.use(cors());
        app.use(helmet());
        app.use(bodyParser.json());
        // app.use(fileUpload()); // se mantienen los archivos guardados
        //Set all routes from routes folder
        app.use("/", routes);

        httpServer.listen(app.get('port'), () => {
            console.log('Corre por puerto: ', app.get('port'));
        });
    })
    .catch(error => console.log(error));
