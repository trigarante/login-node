import {getRepository} from 'typeorm';
import {UsuarioView} from '../entity/TI/Auth/Auth';

export const findUser = async (email) => {
    const userRepository = getRepository(UsuarioView);
    try {
        return await userRepository.findOneOrFail({where: {usuario: email, estado: 1}})
    } catch (e) {
        return e;
    }
}
