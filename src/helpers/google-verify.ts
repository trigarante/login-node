import {OAuth2Client} from "google-auth-library";
import config from "../config/config";
import {Usuarios} from "./../entity/TI/Usuarios";
import {getRepository} from "typeorm";
const client = new OAuth2Client(config.google_id);
export const googleVerify = async (token) => {
    const ticket = await client.verifyIdToken({
       idToken: token,
       audience: config.google_id
    });
    const payload = ticket.getPayload();
    const {email} = payload
    const usuariosRepository = getRepository(Usuarios);
    const usuario = await usuariosRepository.findOneOrFail({
        usuario: email,
    })
    if (Number(usuario.idCorreo) === 0) {
            usuariosRepository.update(usuario.id, {
                idCorreo: Number(ticket.getPayload().sub),
                imagenURL: ticket.getPayload().picture,
                nombre: ticket.getPayload().name,
            });
    }
    return {email};
}


