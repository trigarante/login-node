import { Router } from "express";
import loginRouter from "./Auth/loginRouter";
const routes = Router();

routes.use("/login", loginRouter);

export default routes;
