
/**
 * Ruta: /auth1
 */
import { Router } from 'express';
import UserViewController from "../../controllers/Auth/Auth1Controller"
const router = Router();

router.post("/", UserViewController.login);
router.get("/:id", UserViewController.getEstadoSesionById);
router.put("/logout", UserViewController.logout);
router.put("/cambio-estado", UserViewController.cambioEstado);
export default router;
