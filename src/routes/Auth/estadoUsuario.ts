/**
 *  Ruta: /estadoUsuario
 * */

import { Router } from "express";
import EstadoUsuarioController from "../../controllers/Auth/EstadoUsuarioController";
const router = Router();

router.get("/", EstadoUsuarioController.getEstadoUsuario);
router.put("/fin-estado/:idEstadoUsuario", EstadoUsuarioController.finEstado);
router.post("/", EstadoUsuarioController.postEstadoUsuario);
router.put("/:id", EstadoUsuarioController.updateEstadoUsuario);
router.delete("/:id", EstadoUsuarioController.deleteEstadoUsuario);

export default router;
