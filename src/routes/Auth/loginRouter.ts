import { Router } from "express";
import auth1 from './auth1';
import estadoUsuario from './estadoUsuario';

const router = Router();

router.use('/auth1', auth1);
router.use('/estadoUsuario', estadoUsuario);

export default router;
