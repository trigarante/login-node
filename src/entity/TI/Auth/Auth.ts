/**
 *   Modelo de la vista de usuariosView
 * */
import {
    Entity,
    PrimaryGeneratedColumn,
    Column, 
  } from "typeorm";

  @Entity()
export class UsuarioView {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  idCorreo: string;

  @Column()
  idGrupo: number;

  @Column()
  idTipo: number;

  @Column()
  idSubarea: number;

  @Column()
  idPermisosVisualizacion: number;

  @Column()
  usuario: string;

  @Column()
  password: string;

  @Column()
  fechaCreacion: Date;

  @Column()
  datosUsuario: string;

  @Column()
  estado: number;

  @Column()
  idEmpleado: number;

  @Column()
  grupoUsuarios: string;

  @Column()
  permisos: string;

  @Column()
  descripcionGrupo: string;

  @Column()
  tipoUsuario: string;

  @Column()
  asignado: number;

  @Column()
  imagenUrl: string;

  @Column()
  nombre: string;

  @Column()
  token: string;

  @Column()
  idPuesto: number;

  @Column()
  idTipoSubarea: number;

  @Column()
  extension: string;

}