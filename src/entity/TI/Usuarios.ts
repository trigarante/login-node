import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Usuarios {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    idCorreo: number;

    @Column()
    idGrupo: number;

    @Column()
    idTipo: number;

    @Column()
    idSubarea: number;

    @Column()
    usuario: string;

    @Column()
    estado: number;

    @Column()
    imagenURL: string;

    @Column()
    nombre: string;

    @Column()
    extension: number;
}
