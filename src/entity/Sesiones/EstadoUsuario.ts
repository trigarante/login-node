import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
} from "typeorm";

@Entity()
export class EstadoUsuario {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    idSesionUsuario: number;

    @Column()
    idEstadoSesion: number;

    @Column()
    idMotivosPausa: number;

    @Column()
    fechaInicio: Date;

    @Column()
    fechaFinal: Date;

}
