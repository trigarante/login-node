import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
} from "typeorm";
@Entity()
export class EstadoSesion {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    descripcion: string;

    @Column()
    activo: number;

}
