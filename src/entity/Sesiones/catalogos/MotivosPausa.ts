import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
} from "typeorm";
@Entity()
export class MotivosPausa {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    descripcion: string;

    @Column()
    activo: number;

    @Column()
    tiempo: string;

}
