import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
} from "typeorm";

@Entity()
export class SesionUsuarios {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    idUsuario: number;

    @Column()
    fechaInicio: Date;

    @Column()
    fechaFin: Date;

    @Column()
    idEstadoSesion: number;
}
