import { Request, Response } from "express";
import { EstadoUsuario } from "../../entity/Sesiones/EstadoUsuario";
import { getRepository } from "typeorm";

class EstadoUsuarioController {
    static getEstadoUsuario = async (req: Request, res: Response) => {
        const estadoUsuarioRepository = getRepository(EstadoUsuario);
        try {
            const estadosUsuarios = await estadoUsuarioRepository.find();
            if(estadosUsuarios.length === 0) {
                return res.status(404).json({
                   msg: 'No existen registros en esta tabla'
                });
            }
            res.json({
               estadosUsuarios
            });
        } catch (e) {
            console.log(e);
            return res.status(500).json({
               msg: 'Error inesperado revisar consola del back'
            });
        }
    };

    static finEstado = async (req: Request, res: Response) => {
        const idEstadoUsuario = req.params.idEstadoUsuario;
        const estadoUsuarioRepository = getRepository(EstadoUsuario);
      try{
          const estadoUsuario = await estadoUsuarioRepository.findByIds(idEstadoUsuario);
          if(estadoUsuario.length === 0) {
              return res.status(404).json({
                  msg: 'No se encontro un estado con el id proporcionado'
              });
          }
          await estadoUsuarioRepository.update(idEstadoUsuario, {fechaFinal: new Date()});
          res.json({
              msg: 'Pausa finalizada',
          });

      } catch (e) {
          console.log(e)
          return res.status(500).json({
              ok: false,
              msg: 'error revisar consola del back'
          });
      }
    };

    static postEstadoUsuario = async (req: Request, res: Response) => {
      let estadoUsuario: EstadoUsuario;
      estadoUsuario = req.body
      const estadoRepository = getRepository(EstadoUsuario);
      try {
          await estadoRepository.save(estadoUsuario);
          res.json({
             msg: 'Estado Usuario creado',
             estadoUsuario
          });
      } catch (e) {
          console.log(e);
          return res.status(500).json({
              ok: false,
              msg: 'Ocurrio un error, revisar consola del back'
          });
      }
    };

    static updateEstadoUsuario = async (req: Request, res: Response) => {
      const idEstadoUsuario = req.params.id;
      const estadoRepository = getRepository(EstadoUsuario);
      try {
          const estadoUsuario = await estadoRepository.findByIds(idEstadoUsuario);
          if(estadoUsuario.length === 0) {
              return res.status(404).json({
                  msg: 'No se encontro un estado con el id proporcionado'
              });
          }
          await estadoRepository.update(idEstadoUsuario, {
              idSesionUsuario: req.body.idSesionUsuario,
              idEstadoSesion: req.body.idEstadoSesion,
              // fecha: req.body.fecha,
              idMotivosPausa: req.body.idMotivosPausa
          })
          res.json({
              msg: 'Registro modificado',
          });
      } catch (e) {
          console.log(e);
          return res.status(500).json({
              msg: 'Error revisar consola del back'
          });
      }
    };

    static deleteEstadoUsuario = async (req: Request, res: Response) => {
        const idEstadoUsuario = req.params.id;
        const estadoRepository = getRepository(EstadoUsuario);
        try{
            const estadoUsuario = await estadoRepository.findByIds(idEstadoUsuario);
            if(estadoUsuario.length === 0) {
                return res.status(404).json({
                    msg: 'No se encontro un estado con el id proporcionado'
                });
            }
            await estadoRepository.delete(idEstadoUsuario);
            res.json({
               msg: 'Estado Usuario eliminado permanentemente'
            });
        }catch (e) {
            console.log(e);
            return res.status(500).json({
                msg: 'Error revisar consola del back'
            });
        }
    };
}

export default EstadoUsuarioController
