import { Request, Response } from "express";
import {googleVerify} from "../../helpers/google-verify";
import { findUser } from "../../helpers/findUser";
import config from "../../config/config"
import * as jwt from "jsonwebtoken";
import { getRepository} from "typeorm";
import {SesionUsuarios} from "../../entity/Sesiones/SesionUsuarios";

class UserViewController {
    /**
     *  Función que recibe el token de google, se verifica dicho token, posteriormente se extrae el correo logueado del payload del token,
     *  y con ese correo se busca al usuario en la tabla de Usuarios. Si el usuario tiene activo = 0 o no cuenta con idEmpleado, se retorna un error.
     *  Adicionalmente cuando se pasan estas validaciones en la tabla de sesionUsuarios se guarda la hora en que se logueo el usuario, y por default
     *  se setea el idEstadoSesion = 1
     * */

    static login = async(req: Request, res: Response) => {
        const googleToken = req.body.token;
        try {
            const {email} = await googleVerify(googleToken);
            const user = await findUser(email);
            if(user.name === 'EntityNotFound' || !user.idEmpleado) {
                return res.status(404).json({
                    ok: false,
                    msg: 'usuario no encontrado'
                });
            }
            let sesion = new SesionUsuarios();
            const idUser = user.id;
            sesion.idUsuario = idUser;
            sesion.idEstadoSesion = 1;
            // sesion.fechaInicio = req.body.inicioSesion;
            sesion.fechaFin = null;
            const sesionUsuarioRepository = getRepository(SesionUsuarios);
            const token = jwt.sign({id: idUser},config.jwtSecret, {expiresIn: '6h'});
            const sesionSinCerrar = await sesionUsuarioRepository.find({
                where: [
                    {idUsuario: idUser, idEstadoSesion: 1, fechaFin: null},
                    {idUsuario: idUser, idEstadoSesion: 2, fechaFin: null}
                    ]
            });
            if (sesionSinCerrar.length > 0) {
                sesionSinCerrar.forEach(async (sesion) => {
                   await sesionUsuarioRepository.update(sesion.id.toString() ,{
                      idEstadoSesion: 5,
                   });
                });
            }

            await sesionUsuarioRepository.save(sesion);
            res.json({
                ok: true,
                user,
                token,
                sesion,
                sesionSinCerrar
            });
        } catch (e) {
            console.log(e);
            return res.status(500).json({
               ok: false,
               msg: 'Error inesperado, revisar consola'
            });
        }
    };

    /**
     *  Se obtiene el estado de la sesion mediante el id de la sesion
     * */

    static getEstadoSesionById = async (req: Request, res: Response) => {
      const idSesion = req.params.id;
      const sesionUsuariorepository = getRepository(SesionUsuarios);
      try {
          const sesion = await sesionUsuariorepository.findByIds(idSesion);
          if(sesion.length === 0) {
              return res.status(404).json({
                  msg: 'No se encontro un estado con el id proporcionado'
              });
          }
          res.json({
             sesion
          });
      }
      catch (e) {
          console.log(e);
          return res.status(500).json({
              msg: 'Error revisar consola del back'
          });
      }
    };

    /**
     *  Se cambia el idEstado sesion en la tabla de  sesion Usuarios
     * */

    static cambioEstado = async (req: Request, res: Response) => {
      const idSesion = req.body.id;
      const sesionUsuariorepository = getRepository(SesionUsuarios);
      try {
          const sesion = await sesionUsuariorepository.findByIds(idSesion.toString());
          if(sesion.length === 0) {
              return res.status(404).json({
                  msg: 'No se encontro un estado con el id proporcionado'
              });
          }
          const idEstado = req.body.idEstadoSesion;
          await sesionUsuariorepository.update(idSesion.toString(), {idEstadoSesion: idEstado});
          res.json({
              msg: 'Cambio de estado registrado en base',
          });
      }
      catch (e) {
          console.log(e);
          return res.status(500).json({
              msg: 'Error revisar consola del back'
          });
      }

    };

    /**
     *  Se realiza el logout registrando en base la hora de deslogueo y seteando el idEstadoSesion = 5
     * */
    
    static logout = async(req: Request, res: Response) => {
        const idSesion = req.body.id;
        const sesionUsuarioRepository = getRepository(SesionUsuarios);
      try {
          const sesion = await sesionUsuarioRepository.findByIds(idSesion.toString());
          if(sesion.length === 0) {
              return res.status(404).json({
                  msg: 'No se encontro un estado con el id proporcionado'
              });
          }
          await sesionUsuarioRepository.update(idSesion.toString(), { idEstadoSesion: 5, fechaFin: req.body.fechaFin});
          res.json({
              msg: 'logout registrado en base',
          });
      }  catch (e) {
          console.log(e);
          return res.status(500).json({
              msg: 'Error revisar consola del back'
          });
      }
        
    };
}

export default UserViewController

